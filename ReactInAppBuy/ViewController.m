//
//  ViewController.m
//  ReactInAppBuy
//
//  Created by LUNA on 2017. 10. 25..
//  Copyright © 2017년 Dooboo. All rights reserved.
//

#import "ViewController.h"

#import <StoreKit/StoreKit.h>


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionPurchaseItem500:(id)sender {



        SKPayment *payment = [SKPayment paymentWithProduct:validProducts[0]];

        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        [[SKPaymentQueue defaultQueue] addPayment:payment];

    //  SKProduct *item500 = [SKPay]
    //  SKMutablePayment *myPayment = [SKMutablePayment paymentWithProduct: @"react.iap.consum.500"];
    //  myPayment.quantity = 1;
    //  [[SKPaymentQueue defaultQueue] addPayment:myPayment];
}

- (IBAction)actionRequestProdList:(id)sender {
    // 이렇게 제품 이름을 넣고.
    NSSet *productIdentifiers = [NSSet setWithObjects:@"react.iap.consum.500", @"react.iap.consum.1000", nil];

    NSLog(@"  Requesting Product List with  %@", productIdentifiers);

    productsRequest = [[SKProductsRequest alloc]
                       initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
}

#pragma mark ===== StoreKit Delegate

-(void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction *transaction in transactions) {
        NSData *newReceipt = transaction.transactionReceipt;

        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
                break;
            case SKPaymentTransactionStatePurchased:
            NSLog(@"Purchasing : receipt : %@", transaction.transactionReceipt);
                //        if ([transaction.payment.productIdentifier
                //             isEqualToString:productID]) {
                //          NSLog(@"Purchased ");
                //          UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                //                                    @"Purchase is completed succesfully" message:nil delegate:
                //                                    self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                //          [alertView show];
                //        }
                       [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                NSLog(@"Restored ");
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                NSLog(@"Purchase failed ");
                break;
            default:
                break;
        }
    }
}


-(void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    SKProduct *validProduct = nil;
    long count = [response.products count];

    NSLog(@"productsRequest:(SKProductsRequest *)request didReceiveResponse ::  %ld", count);

    validProducts = response.products;

    // 이렇게 확인 함..
    for (int k = 0; k < count; k++) {
        validProduct = [response.products objectAtIndex:k];
        NSLog(@"   Product Id : %@", validProduct.productIdentifier);
    }

    if (count > 0) {
        validProduct = [response.products objectAtIndex:0];
//            if ([validProduct.productIdentifier
//                 isEqualToString:productID]) {
//              [productTitleLabel setText:[NSString stringWithFormat:
//                                          @"Product Title: %@",validProduct.localizedTitle]];
//              [productDescriptionLabel setText:[NSString stringWithFormat:
//                                                @"Product Desc: %@",validProduct.localizedDescription]];
//              [productPriceLabel setText:[NSString stringWithFormat:
//                                          @"Product Price: %@",validProduct.price]];
//            }
    } else {
        UIAlertView *tmp = [[UIAlertView alloc]
                            initWithTitle:@"Not Available"
                            message:@"No products to purchase"
                            delegate:self
                            cancelButtonTitle:nil
                            otherButtonTitles:@"Ok", nil];
        [tmp show];
    }
    [activityIndicatorView stopAnimating];
    purchaseButton.hidden = NO;
}

@end
