//
//  ViewController.h
//  ReactInAppBuy
//
//  Created by LUNA on 2017. 10. 25..
//  Copyright © 2017년 Dooboo. All rights reserved.
//

#import <StoreKit/StoreKit.h>

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <SKProductsRequestDelegate,SKPaymentTransactionObserver>
{
  SKProductsRequest *productsRequest;
  NSArray *validProducts;
  UIActivityIndicatorView *activityIndicatorView;
  IBOutlet UILabel *productTitleLabel;
  IBOutlet UILabel *productDescriptionLabel;
  IBOutlet UILabel *productPriceLabel;
  IBOutlet UIButton *purchaseButton;
}
@end

