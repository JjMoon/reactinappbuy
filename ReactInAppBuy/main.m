//
//  main.m
//  ReactInAppBuy
//
//  Created by LUNA on 2017. 10. 25..
//  Copyright © 2017년 Dooboo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
